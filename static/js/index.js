function freshenUp(element){
    let isDirty = element.classList.toggle("dirty");
}

window.onload = () => {
    let rosette = document.getElementById("rosette");
    let height = rosette.offsetHeight;

    rosette.style = `width: ${height}px`;
};